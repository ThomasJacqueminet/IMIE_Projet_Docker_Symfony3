<?php
// src/AppBundle/Command/GreetCommand.php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UsersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('users:show')
            ->setDescription('Display all users');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $schema = $this->getApplication()->getKernel()->getContainer()->get('app_user');
        $users = $schema->getAllUsers();
        $output->writeln($users);
    }
}