<?php
namespace AppBundle\Schema;

use Doctrine\ORM\EntityManager;

class Schema
{
    /** @var EntityManager $em */
    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getAllUsers()
    {
        $users = $this->em->getRepository('AppBundle:User')->findAll();

        return $users;
    }
}