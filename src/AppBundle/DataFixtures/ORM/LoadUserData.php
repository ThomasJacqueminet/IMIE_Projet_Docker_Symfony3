<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 27/05/2016
 * Time: 15:57
 */
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setPassword('test');
        $userAdmin->setEmail('admin@test.com');

        $userTest = new User();
        $userTest->setUsername('Testeur');
        $userTest->setPassword('test');
        $userTest->setEmail('testeur@test.com');

        $userThomas = new User();
        $userThomas->setUsername('Thomas');
        $userThomas->setPassword('test');
        $userThomas->setEmail('Thomas@test.com');

        $userJeanPaul = new User();
        $userJeanPaul->setUsername('Jean-Paul');
        $userJeanPaul->setPassword('test');
        $userJeanPaul->setEmail('jpaul@test.com');


        $manager->persist($userAdmin);
        $manager->persist($userTest);
        $manager->persist($userThomas);
        $manager->persist($userJeanPaul);
        $manager->flush();
    }
}